# Google Maps Plugin

This plugin is developed for [Typemill](https://github.com/typemill/typemill), a flat-file cms.

### How to install

* Download this plugin (zip).
* Unzip the plugin.
* Upload the plugin folder into to the typemill plugin-folder (`typemill/plugins/`)
* Login to your Typemill installation.
* Go to settings -> plugins and activate your plugin.

### Insert google maps into a page

* Create paragraph
* Insert shortcode `[:GOOGLEMAPS address="Borkum, Germany" :]`
  * with zoom `[:GOOGLEMAPS address="Borkum, Germany" zoom=15 :]`

### Screenshots

#### Embedded map

![Map Preview](https://raw.githubusercontent.com/turbopixel/typemill-googlemaps/master/github/screenshot-map.png "Map Preview")

#### Plugin settings

![Settings](https://raw.githubusercontent.com/turbopixel/typemill-googlemaps/master/github/screenshot-settings.png "Settings")